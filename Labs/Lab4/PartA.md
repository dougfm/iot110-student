[LAB4 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/setup.md)

## Part A
**Synopsis:** We need to ensure that the I2C serial bus capability is enabled and
functional with its connection to the BMP280 pressure/temperature sensor.  We
will enable the I2C from RPi settings and then install some ```i2c-tools``` to scan
all of the I2C addresses for the presence of the BMP280.   From the BMP280
datasheet we know that the I2C bus address is either 0x76 (SDO grounded) or 0x77
(SDO connected high or left floating).  This will show up and be apparent using
the ```i2cdetect``` utility.

## Objectives
* Enabling I2C Bus Capability on RPI
* Wire the BMP280 into the breadboard
* Test to ensure proper connectivity

*Note:* We'd like to thank **Matt Hawkins** for his excellent tutorial on using Python
for many sensor devices on RPI and his example code for the BME280 (similar device to the BMP280).

[Python Driver BME280](http://www.raspberrypi-spy.co.uk/2016/07/using-bme280-i2c-temperature-pressure-sensor-in-python/)<br />
[Enable I2C Interface](http://www.raspberrypi-spy.co.uk/2014/11/enabling-the-i2c-interface-on-the-raspberry-pi/)<br />
[BMP280 Datasheet](https://cdn-shop.adafruit.com/datasheets/BST-BMP280-DS001-11.pdf)<br />

## Enabling I2C Bus Capability on RPI

### Step A1: Enable i2c using raspi-config utility
```
pi$ sudo raspi-config
```
Now complete the following steps :

* Select “Advanced Options”
* Select “I2C”

The screen will ask if you want the ARM I2C interface to be enabled :

* Select “Yes”
* Select “Ok” (Enable I2C)
* Select “Ok” (Install Kernel Modules)
* Select “Finish” to return to the command line.
* Select Reboot

### Step A2: Enable i2c using RPi Preferences Panel
Go to Menu > Preferences > Raspberry Pi Configuration and select the Intefaces Tab
<img src="https://gitlab.com/iot110/iot110-student/raw/master/Labs/Lab4/images/I2C_800x480.png" alt="I2C Enable">

### Step A3: – Install I2C Utilities

```
pi$ sudo apt-get update
pi$ sudo apt-get install -y python-smbus i2c-tools
```

### Step A4: Reboot and Test I2C
```
pi$ sudo reboot   
  => wait until rebooted
pi$ lsmod | grep i2c_
i2c_bcm2708             4834  0
i2c_dev                 5859  0
```
That will list all the modules starting with “i2c_”. If it lists “i2c_bcm2708” then the module is running correctly.

### Step A5: Wire the BMP280 on the Breadboard
The following breadboard configuration will be used to test the BMP280.  Please
add the BMP280 module and connect as showing below.
![Lab4 Breadboard](https://gitlab.com/iot110/iot110-student/raw/master/Labs/Lab4/images/Lab4-Breadboard.png)

### Step A6: Test the I2C Address
Open the [BMP280 Datasheet](https://cdn-shop.adafruit.com/datasheets/BST-BMP280-DS001-11.pdf) and browse to section **5.2 I²C Interface**<br />

The **I²C interface** uses the following pins:
* SCK: serial clock (SCL)
* SDI: data (SDA)
* SDO: Slave address LSB (GND = ‘0’, VDDIO = ‘1’)

"...The 7-bit device address is 111011x. The 6 MSB bits are fixed. The last bit is changeable by
SDO value and can be changed during operation. Connecting SDO to GND results in slave
address 1110110 (**0x76**); connection it to VDDIO results in slave address 1110111 (**0x77**), which is the same as BMP180’s I²C address. The SDO pin cannot be left floating; if left floating, the
I²C address will be undefined."

**Connect SDO to 10K PU**
```
pi$ sudo i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- 77 --
```

**Connect SDO to GND**

```
pi$ sudo i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- 76 --
```

[PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartB.md) Create BMP280 Driver

[LAB4 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/setup.md)
