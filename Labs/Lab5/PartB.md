[LAB5 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/setup.md)

## Part B - Send the Sensor Data Object to the Webapp
**Synopsis:** In order to communicate sensor data from our Python driver program
and main web server, we need to package it in a Python dictionary structure and 
sent it via Server Sent Events as a single data item and then decode it into a
JavaScript Object.

## Objectives
* Point to new lab5.css and lab5.js file after renaming them for Lab5
* Test that the single quoted text sensor structure arrives at the web
* Convert single quoted strings to double quoted (required by JSON.parse)
* Convert the sensor JSON data into a JavaScript object

### Step B1: Adjust index.html <head> section for lab5.css and lab5.js name change
```
<head>
  <meta charset="utf-8">
  <title>UW IoT Lab5</title>
  <link rel="stylesheet" type="text/css" href="{{ url_for('static',filename='css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url_for('static',filename='css/lab5.css') }}">
  <script src="{{ url_for('static',filename='js/jquery-3.1.1.min.js') }}"></script>
  <script src="{{ url_for('static',filename='js/bootstrap.min.js') }}"></script>
  <script src="{{ url_for('static',filename='js/lab5.js') }}"></script>
</head>
```

### Step B2: Adjust lab5.js iotSource.onmessage function for initial test
```python
iotSource.onmessage = function(e) {
  console.log(e.data);
}
```

### Step B3: Start Webserver and Verify that IoT sensor text data is sent
Start Webserver
```sh
pi$ ./main.py
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger pin code: 269-766-075
   192.168.1.17 - - [11/Feb/2017 17:14:34] "GET /myData HTTP/1.1" 200 -
```

Using Google Chrome Developer Tools (right click 'inspect') observe console
The following should be what the text version of the sensor object looks like.
```
{'led_red': '1', 'chip_id': 88, 'led_grn': '1', 'temperature': {'units': 'C', 'reading': 25.41}, 'chip_version': 1, 'pressure': {'units': 'hPa', 'reading': 1018.8871169722084}, 'switch': '0', 'led_blu': '1'}

```
### Step B4: convert the sensor JSON data into a JavaScript object
```JavaScript
iotSource.onmessage = function(e) {
  // must convert all single quoted data with double quote format
  var dqf = e.data.replace(/'/g, '"');
  // now we can parse into JSON
  d = JSON.parse(dqf);
  console.log(d);
}
```

Verify that we now have a JavaScript representation of the sensor object in the
Chrome Console.
![JavaScript Object](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/JS_SensorObject.png)


[PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/PartC.md) Display Time, Temp and Pressure in a Bootstrap Table Structure

[LAB5 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/setup.md)
