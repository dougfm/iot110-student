[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)

### Part C - Organize and Code Web Assets. ###
**Synopsis:** Before we finish up on the web JavaScript client side code and
styling via CSS, we want to better organize our assets by moving the scripts
into a separate file called ```js/lab3.js```.  We also want to create a style sheet
for this lab called ```css/lab3.css```.  The files go into their respective
folders under the ```static``` folder.

#### Step C1: Flask Parameter Passing w/o SSE
**Objective:** Flask uses a template engine called
[Jinja2](http://jinja.pocoo.org).  This framework is used to pass variables and
execute script code within HTML as it renders within a web browser.  
In our lab ```main.py``` web server the index.html is rendered on the default
path in the following code snippet.  The index() method is called when the web
page reloads on the default path ("/"). The default path is executed whenever a
CURL is executed (e.g. iot1234:5000) or when you type your host address into a
web browser URL address line (e.g. URL=>iot1234:5000). In our original main.py, in
the execution of index(), the GPIO driver is instantiated and the initial state 
of the led and switch variables are read and passed as arguments to 
the ```render_template()``` function.

```python
@app.route("/")
def index():
    # create an instance of my pi gpio object class.
    pi_gpio = PiGpio()
    switch_state = pi_gpio.read_switch()
    led1_state = pi_gpio.get_led(1)
    led2_state = pi_gpio.get_led(2)
    led3_state = pi_gpio.get_led(3)
    return render_template('index.html', switch=switch_state,
                                led1=led1_state,
                                led2=led2_state,
                                led3=led3_state)
```

#### Step C2: Flask Parameter Passing with SSE
In our new *Server Sent Event* implementation for this lab, we will push all of
this state information from the server by continuously reading the state. We will 
have a different mechanism for communicating and rendering the state to our web 
page using JavaScript.  This will simplify the index.html rendering code as 
follows:
```html
@app.route("/")
def index():
    return render_template('index.html')
```

#### Step C3: Adding the Client JavaScript Code to connect to SSE
In Part B we added an SSE data publishing endpoint to ```main.py``` and a simple
console.log() debug output of the client side subscription to this endpoint.  
Now we need that JavaScript, at the bottom of the index.html code, to change the
HTML code dynamically to reflect the state of the GPIO pins.  We start by
ensuring we have state variables for the leds that will be retained.  We read
and parse the incoming data and then call respective functions to update the
switch and leds on the HTML webpage.  These updates are accomplished using 
jQuery methods.  (e.g. the $().method capability we added in Part A).

```python
      /* start executing only after document has loaded */
      $(document).ready(function() {
        /* establish global variables for LED status */
        var led1;
        var led2;
        var led3;

 		/* var iotSource = new EventSource("{{ url_for('myData') }}"); */
        /* intercept the incoming states from SSE */
        iotSource.onmessage = function(e) {
          var params = e.data.split(' ');
          updateSwitch(params[0]);
          updateLeds(1,params[1]);
          updateLeds(2,params[2]);
          updateLeds(3,params[3]);
        }

        /* update the Switch based on its SSE state monitor */
        function updateSwitch(switchValue) {
          if (switchValue === '1') {
            $('#switch').text('ON');
          } else if (switchValue === '0') {
            $('#switch').text('OFF');
          }
        }

        /* update the LEDs based on their SSE state monitor */
        function updateLeds(ledNum,ledValue) {
          if (ledNum === 1) {
            if (ledValue === '1') {
              $('#red_led_label').text('ON');
              led1 = "ON"
            } else if (ledValue === '0') {
              $('#red_led_label').text('OFF');
              led1 = "OFF"
            }
          }
          else if (ledNum === 2) {
            if (ledValue === '1') {
              $('#grn_led_label').text('ON');
              led2 = "ON"
            } else if (ledValue === '0') {
              $('#grn_led_label').text('OFF');
              led2 = "OFF"
            }
          }
          else if (ledNum === 3) {
            if (ledValue === '1') {
              $('#blu_led_label').text('ON');
              led3 = "ON"
            } else if (ledValue === '0') {
              $('#blu_led_label').text('OFF');
              led3 = "OFF"
            }
          }
        }
```

At this point we have a fully functional page, but perhaps not a very 
esthetically pleasing dashboard.  In the next part we will fix that with the
Twitter Bootstrap framework we prepared in Part A.

[PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartD.md) Add Bootstrap UI Elements and CSS Styles.

[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)
