[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)

### Part A - Add Twitter Bootstrap Framework ###
**Synopsis:** A good website design provides for *separation of concerns*.  This
means that the HTML is generally separated from the CSS which is separated from
the JavaScript in so much as the code it placed in separate files and folders.

#### Step A1: Create Expanded Web File Structure
**Objective:** Flask provides for a traditional web site layout, where content (HTML)
is separated from presentation (CSS) and action (JavaScript) by placing each of
these files in their own separate folders. In addition to the ```templates```
folder we need to add a ```static``` folder and then add a ```css``` and ```js```
folder under the static *assets* folder.

```sh
pi$ cd ; cd Documents/GIT_REPOS/iot110/Labs
pi$ mkdir Lab3 ; cd Lab3
pi$ mkdir -p webapp/templates ; mkdir -p webapp/static/css ; mkdir -p webapp/static/js
pi$ cd webapp/static/css ; touch lab3.css
pi$ cd ../js ; touch lab3.js
```

#### Step A2: Download bootstrap and jQuery
**Objective:** We want to install the Bootstrap UI and components and put them
in our static assets folder. Use the links below to download the latest version
of Bootstrap and jQuery and then we will copy a few select files into the file
structure we created above.

[Download Bootstrap](http://getbootstrap.com/getting-started/#download)

The following is the jQuery site, but we will directly download using wget below:
[Download jQuery](http://jquery.com/download)

```sh
pi$ wget https://code.jquery.com/jquery-3.1.1.min.js
```
Now, let's grab two files from the Bootstrap download folder.  At the time of
this lesson it was /Downloads/bootstrap-3.3.7-dist/ (or wherever your Downloads
  folder happens to reside).

**Note:** The code below is copying from the HOST downloads folder thru the mount
point to the RPi's GIT repo mounted folder.  (Adjust this for  your unique
mount point situation)
```sh
host$ cd Downloads/bootstrap-3.3.7-dist
host$ cp css/bootstrap.min.css ~/PI_MOUNT_8e3c/iot110/Labs/Lab3/webapp/static/css/
host$ cp js/bootstrap.min.js ~/PI_MOUNT_8e3c/iot110/Labs/Lab3/webapp/static/js/

# Note: just as a reminder (MacOSX) the command for mounting your PI is
host$ sshfs pi@iot1234:Documents/GIT_REPOS PI_MOUNT_1234/ -C
```
#### Step A3:  
We will need to adjust the ```index.html``` meta and script sections to account
for our organization of the *web assets*.  Modify the HEAD section of the HTML
to make this change according to the Flask provisions for static assets.
```html
<head>
  <meta charset="utf-8">
    <title>UW IoT Lab3</title>
    <link rel="stylesheet" type="text/css" href="{{ url_for('static',filename='css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url_for('static',filename='css/lab3.css') }}">
    <script src="{{ url_for('static',filename='js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url_for('static',filename='js/bootstrap.min.js') }}"></script>
  </head>
```

OK, were done with the organization of our assets and prepared for styling with
Bootstrap components and actions.  Next we will move on to adding dynamic Server
Sent Events capability to our code.

[PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartB.md) Add additional
capbility to main.py for *Server Sent Events*.

[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)
