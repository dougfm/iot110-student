[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)


### Part D - Add Bootstrap UI Elements and CSS Styles. ###
**Synopsis:** Adding Bootstrap.  We will add the following modifications to our
main index.html file to be compatible with Bootstrap UI.  

```html
<body>
  <div class="container panel">
    <div class="container well well-sm">
      <h3 id='title'>Lab 3 GPIO Server Sent Events</h3>
    </div>

    <div class="container well">
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
          <h4 class="gpio">Switch</h4>
        </div>
        <div class="col-sm-3 switch">
          <h4><span id="switch" class="label label-default">&#9759</span></h4>
        </div>
        <div class="col-sm-2">
          <h4 class="gpio">[GPIO27]</h4></div>
        <div class="col-sm-2"></div>
      </div>

      <!-- LED: RED -->
      <div class="row">
        <div class="col-sm-2"></div>

        <div class="col-sm-3 led_button">
          <button type='button' id="red_led_btn" class="btn btn-md btn-default">Toggle LED</button>
        </div>
        <div class="col-sm-3 led_label">
          <h4><span id="red_led_label" class="label label-default">&#9728</span></span></h4>
        </div>
        <div class="col-sm-2">
          <h4 class="gpio">[GPIO18]</h4></div>
        <div class="col-sm-2"></div>
      </div>
      <!-- LED: GREEN -->
      <div class="row">
        <div class="col-sm-2"></div>

        <div class="col-sm-3 led_button">
          <button type='button' id="grn_led_btn" class="btn btn-md btn-default">Toggle LED</button>
        </div>
        <div class="col-sm-3 led_label">
          <h4><span id="grn_led_label" class="label label-default">&#9728</span></h4>
        </div>
        <div class="col-sm-2">
          <h4 class="gpio">[GPIO13]</h4></div>
        <div class="col-sm-2"></div>
      </div>
      <!-- LED: BLUE -->
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3 led_button">
          <button type='button' id="blu_led_btn" class="btn btn-md btn-default">Toggle LED</button>
        </div>
        <div class="col-sm-3 led_label">
          <h4><span id="blu_led_label" class="label label-default">&#9728</span></h4>
        </div>
        <div class="col-sm-2">
          <h4 class="gpio">[GPIO23]</h4></div>
        <div class="col-sm-2"></div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var iotSource = new EventSource("{{ url_for('myData') }}");
    // intercept the incoming states from SSE
  </script>
</body>
```

Now, we will add our Cascading Style Sheet styling to our site.  

```css
body {
  background-color: rgb(51, 0, 111);
  width: 800px;
  margin:5px auto;
}

span.label  {
    font-size: 1.3em;
}

.btn {
  font-size: 1.2em;
}

button {
  margin: 5 auto;
}

h4.gpio {
  margin:13 auto;
}
.well {
  width: 100%;
}

.well-sm {
  margin: 10px auto;
}

.panel {
  background-color: darkgray;
  width: 75%;
  text-align: center;
}
```

[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)
